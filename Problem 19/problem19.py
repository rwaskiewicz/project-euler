#Desc:    Solution for Problem 19 (Counting Sundays) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    8/18/2013
#Problem: How many Sundays fell on the first of the month during the twentieth century 
#		  (1 Jan 1901 to 31 Dec 2000)?

#create a dictionary where the keys correspond to months, and the
#the value corresponds to the number of days in that month.  
#Let January = 0, February = 1, ... December = 11
def createMonthDict():
	monthDict = {}
	monthDict.update(dict.fromkeys([0, 2, 4, 6, 7, 9, 11], 31))
	monthDict.update(dict.fromkeys([3, 5, 8, 10], 30))
	monthDict.update(dict.fromkeys([1], 28))
	return monthDict

def main():
	currentDay = 2			#Jan 1 1901 was a Tuesday
	currentMonth = 0		#Beginning with January 1901
	firstDaySunday = 0		#Count of days that the first of the month was on a Sunday
	monthDayDict = createMonthDict()
	
	for x in range(0, 1200):
		daysAdded = monthDayDict[currentMonth]
		actualYear = (x/12)+1900
		leapYearCheck = actualYear%400==0 and actualYear%100!=0
		if (currentMonth and leapYearCheck):
			daysAdded+=1	
		currentDay += daysAdded
		if (currentDay % 7 == 0):
			firstDaySunday += 1
		currentMonth += 1
		currentMonth %= 12	
	print firstDaySunday

main()
		