# Desc: Solution for Problem 2 (Even Fibonacci Numbers) on Project Euler
# Date: 7/17/2013
# Problem: By considering the terms in the Fibonacci sequence whose values do not exceed 
#		   four million, find the sum of the even-valued terms.

firstTerm=1
secondTerm=2
evenSum=secondTerm

while 1:
	result=firstTerm+secondTerm
	if (result>4000000):
		break
	if (result%2==0):
		evenSum+=result
	firstTerm=secondTerm
	secondTerm=result
print evenSum