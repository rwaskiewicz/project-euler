#Desc: Solution for Problem 4 (Largest Palindrome Product) on Project Euler
#Author: Ryan Waskiewicz
#Date: 7/28/2013
#Problem: Find the largest palindrome made from the product of two 3-digit numbers.

#The solution here is nothing more than uses an 'optimized' brute force algorithm,
#taking approximately O(n^2) time.  For this exercise, I wanted to start using Python's
#built in unit test's instead of focusing on an optimal solution.

import unittest

#Check to see if the number is a palindrome or not
def isPalindrome(numberStr):
	reverseStr = numberStr[::-1]
	if (numberStr == reverseStr):
		return 1
	return 0

def hasThreeDigits(factor1, factor2):
	return ((factor1/100.0 >= 1) & (factor2/100.0 >= 1))
	
savedProduct = 0
for x in range(999, 499, -1):
	for y in range(999, 499, -1):
		if (isPalindrome(str(x*y)) & (x*y > savedProduct) & hasThreeDigits(x,y)):
			savedProduct = x*y
print savedProduct			

			
class UnitTestClass(unittest.TestCase):
	def testForPalindromes(self):
		self.assertEqual(isPalindrome('1'), True)
		self.assertTrue(isPalindrome('717'))
		self.assertTrue(isPalindrome('121'))
		self.assertTrue(isPalindrome('10001'))
		self.assertFalse(isPalindrome('1321'))
		self.assertFalse(isPalindrome('100000000000000000'))
		
	def testForThreeDigits(self):
		self.assertTrue(hasThreeDigits(100, 100))
		self.assertTrue(hasThreeDigits(1000, 100))
		self.assertTrue(hasThreeDigits(170, 9099))
		self.assertFalse(hasThreeDigits(91, 999))
		self.assertTrue(hasThreeDigits(982, 932))
		

if __name__=='__main__':
	suite = unittest.TestLoader().loadTestsFromTestCase(UnitTestClass)
	unittest.TextTestRunner(verbosity=3).run(suite)