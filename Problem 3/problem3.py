#Desc: Solution for Problem 3 (Largest Prime Factor) on Project Euler
#Author: Ryan Waskiewicz
#Date: 7/17/2013
#Usage: problem3.py <positive int>

#Problem: What is the largest prime factor of the number 600851475143 ?

#Unique Factorization Theorem (UFT): Every positive integer > 1 can be written as a prime
#or the product of 2+ prime factors

#Solution Theory: Using UFT any number can be written as the product of 2+ prime factors.
#However, not every factor of a number is prime, but since any number can be written as
#the product of 2+ prime factors, we can find low(ish) prime factors relatively quickly &
#divide them from our now non-prime number - which can be represented as the product
#of n prime numbers.  This method is similar to algebraic substitution:
#148 = 2 * (74) = 2 * (2 * 37) => 74 = 2 * 37

#Determine if a number is prime or not
def isPrime(x):
	for indice in range(2,x):
		if (x%indice==0):
			return 0;
	return 1;

#Determine if the divisor is a factor or not of the divident
def isFactor(dividend, divisor):
	if (dividend%divisor==0):
		return 1;
	return 0;

#By giving us a number x (600851475143), we know that it's a composite integer.  As such,
#it's greatest prime divisor will be less than sqrt(600851475143)
def findPrimeFactor(compositeInt):
	upperBounds = int(compositeInt ** 0.5);
	#First check for to see if this compositeInt's sqrt is prime
	if (isPrime(upperBounds) and compositeInt%(compositeInt ** 0.5)==0):
		return upperBounds
	for indice in range(2,upperBounds):
		if (isFactor(compositeInt, indice) & isPrime(indice)):
			#Divide the int by the index, which is a factor of the int and prime
			compositeInt/=indice
			#Calculate the largest prime factor of the composite int
			return findPrimeFactor(compositeInt)
	return compositeInt

#Run the script
import sys
primeNumber = 0

if (len(sys.argv)>=2):
	primeNumber=int(sys.argv[1])

if (primeNumber<10 and isPrime(primeNumber)):
	print primeNumber
else:
	primeNumber = findPrimeFactor(primeNumber) 
	print primeNumber
