#Desc: Solution for Problem 5 (Smallest Multiple) on Project Euler
#Author: Ryan Waskiewicz
#Date: 7/18/2013
#Problem: What is the smallest positive number that is evenly divisible by all of the
#		  numbers from 1 to 20?

#Determine if a number is prime or not
def isPrime(x):
	for indice in range(2,x):
		if (x%indice==0):
			return 0;
	return 1;

#Determine if the final product is divisible by each non prime
def testIfDivisible(fProduct,nPrimes,primes):
	for nonPrimeNumber in nPrimes:
 		if (fProduct%nonPrimeNumber != 0):
			fProduct = findLowestPrimeFactor(fProduct, nonPrimeNumber, primes)
	return fProduct

#Find the lowest prime number that we can multiply final product by
#to get the final product divisible by the original non-prime number
def findLowestPrimeFactor(fProduct, nonPrimeNum, primeList):
	for primeNumber in primeList:
		if ((fProduct*primeNumber)%nonPrimeNum==0):
			return fProduct*primeNumber
	#no such number exists
	return fProduct*nonPrimeNum

finalProduct = 1
primeNumbers=[]
nonPrimeNumbers = []

#separate the prime numbers from the non-prime numbers
for x in range(1,21):
	#the final product must be divisible by all primes,
	#since their only factors are 1 and themselves
	if (isPrime(x)):
		finalProduct*=x
		primeNumbers.append(float(x))
	else:
		nonPrimeNumbers.append(float(x))
		
#our answer this far is divisible by all primes, but not necessarily all non-primes
finalProduct = testIfDivisible(finalProduct,nonPrimeNumbers,primeNumbers)	
print finalProduct