#Desc:    Solution for Problem 7 (10001st Prime) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    12/9/2013
#Problem: By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
######### What is the 10001st prime number?

import math

# Determine if a number is prime or not - 
# For all numbers >= 9, check to see if its prime
# by iterating over odd numbers (all even numbers are prime)
# less than the sqrt of x
def isPrime(x):
	if ((x % 2 == 0 and x != 2) or x == 1):
		return 0
	if (x < 9):
		return 1

	squareRootX = int(math.floor(math.sqrt(x)))
	for indice in range(3,squareRootX+1,2):
		if (x%indice==0):
			return 0
	return 1

#Control for counting the number of primes found thus far
def countPrimesThrough(wantedNumberOfPrimes):
	numberOfPrimes = 0
	currentNumber = 1
	while (numberOfPrimes < wantedNumberOfPrimes):

		if (isPrime(currentNumber)):
			numberOfPrimes += 1

		if (numberOfPrimes == wantedNumberOfPrimes):
			print "The prime number is", currentNumber
		
		currentNumber += 1

countPrimesThrough(10001) 