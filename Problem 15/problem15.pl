#Desc:    Solution for Problem 15 (Lattice Paths) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    12/20/2013
#Problem: Starting in the top left corner of a 2×2 grid, and only being able to
######### move to the right and down, there are exactly 6 routes to the bottom 
######### right corner.  How many such routes are there through a 20×20 grid?

use warnings;
use strict;

#count the number of paths a slice of the grid can take, where location
#[0][0] is the max number of routes for a MxN grid
sub countPaths($$)
{
	my $oldColRef = shift;
	my $gridSize = shift;
	
	my $currentValue = 0;	
	my @newCol;

	for (my $i = $gridSize-1; $i >= 0; $i--)
	{
		$currentValue += @$oldColRef[$i];
		$newCol[$i] += $currentValue;
	}
	return \@newCol;
}

#create an Nx1 slice of the grid, where each line intersection has only one 
#path to the bottom right corner.  Then expand the grid until its NxN, counting
#the number of routes that each subset of the grid has to the bottom right
#corner
sub findSol($)
{
	my $gridSize = shift;
	my @currCol = (1)x$gridSize;
	my $currColRef = \@currCol;

	for (my $i = 0; $i < $gridSize; $i++)
	{
		$currColRef = countPaths($currColRef, $gridSize);	
	}

	my $solution = 2*@$currColRef[0];
	print "The number of paths for a size $gridSize array is $solution\n";
}


findSol(20);