#Desc:    Solution for Problem 10 (Summation of Primes) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    12/9/2013
#Problem: The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
######### Find the sum of all the primes below two million.
#Usage:   problem10.py <Upper bound for primes>

import sys
import math

# Determine if a number is prime or not - 
# For all numbers >= 9, check to see if its prime
# by iterating over odd numbers (all even numbers are prime)
# less than the sqrt of x
def isPrime(x):
	if ((x % 2 == 0 and x != 2) or x == 1):
		return 0
	if (x < 9):
		return 1

	squareRootX = int(math.floor(math.sqrt(x)))
	for indice in range(3,squareRootX+1,2):
		if (x%indice==0):
			return 0
	return 1

# Control for prime number calculation
def sumPrimesThrough(max):
	primeSummation = 0
	for number in xrange(1, max):
		if (isPrime(number)):
			primeSummation += number
	print "Calculated prime number:", primeSummation

#Run the script
maxNumber = 1

if (len(sys.argv)>=2):
        maxNumber = int(sys.argv[1])

sumPrimesThrough(maxNumber) 