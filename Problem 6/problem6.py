#Desc: Solution for Problem 6 (Smallest Multiple) on Project Euler
#Author: Ryan Waskiewicz
#Date: 7/19/2013
#Problem: Find the difference between the sum of the squares of the first one hundred 
#		  natural numbers and the square of the sum.

#Using Gaussian summation to find square of sum
squareOfSum = ((100*(100+1))/2)**2

sumOfSquares = (100*(100+1)*(2*100+1))/6
	
print squareOfSum-sumOfSquares