#Desc:     Solution for Problem 8 (Largest Product in a Series) on Project Euler
#Author:   Ryan Waskiewicz
#Date:     8/3/2013
#Problem:  Find the greatest product of five consecutive digits in the 1000-digit number.
#Solution: Based on Rabin-Karp string searching, iterate through the list of numbers, 
#		   multiplying by the next number in the 5 num sequence and dividing by the last
#Note:     Have attached text file '1000digitNumber.txt' in the same dir as this script

#parse the text file, store each number in an array
def parseTextFile():
	fileObj = open('1000digitNumber.txt','rt')
	nextNum = 1
	allNumbers = []
	while (nextNum):
		nextNum = fileObj.read(1)
		if (nextNum == '\n' or nextNum == ''):
			continue
		allNumbers.append(float(nextNum))
	fileObj.close()
	return allNumbers
		
#find the largest product of 5 consecutive digits in an array
def findLargestProduct(allNumbers):
	savedProduct   = 1				#final product
	currentProduct = 1				#in progress value
	indice = 0						#used to iterate through the ist
	resetCount = 0;					#count whether we've reset recently
	while (indice<len(allNumbers)):
		#found a zero. we will have to recalc solution on next 5 passes of lopp
		if (allNumbers[indice] == 0):
			currentProduct = 1
			indice+=1
			resetCount = 0
			continue
		#divde by most left most num only if its in the array and we're not
		#rebuilding the currentProduct (zero detection_
		if (resetCount>=5):
			currentProduct /= allNumbers[indice-5]
		currentProduct *= allNumbers[indice]
		if (currentProduct >= savedProduct):
			savedProduct = currentProduct
		indice+=1
		resetCount += 1
	return savedProduct

def main():		
	numberArray = parseTextFile()
	largestProduct = findLargestProduct(numberArray)
	print '\nLargest product of five consecutive values: %f\n' % (largestProduct)

if __name__ == "__main__":
    main()