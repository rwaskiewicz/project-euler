#Desc:    Solution for Problem 9 (Special Pythagorean triplet) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    8/10/2013
#Problem: There exists exactly one Pythagorean triplet for which a + b + c = 1000.
######### Find the product abc

import math

#test to see if the condition that the sume of the three
#numbers is 1000
def testForPass(a, b, c):
	if (c < b and c < a):
		return 0
	return ((a+b+c)==1000)

#calculate the 'hypotenuse' in the equation, ensuring it
#is a whole number
def calculateHypotenuse(a,b):
	c = math.sqrt(a**2+b**2)
	if (c % 1 != 0):
		c = -1
	return c
	

#search the triple value that will satisfy the 2 requirements
#of the problem
def calculateTriples():
	for a in range(1,500):
		for b in range(2,500):
			c = calculateHypotenuse(a,b)
			passValue = testForPass(a,b,c)
			if (passValue):
				return a*b*c

result = calculateTriples()
print result