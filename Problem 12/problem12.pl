#Desc:    Solution for Problem 12 (Highly divisible triangular number) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    12/19/2013
#Problem: What is the value of the first triangle number to have over five hundred divisors?

use warnings;
use strict;

#let n be the nth triangle number.
#this is equivalent to Gauss' sum
sub generateNthTriangleNum($)
{
	my $triangleNumToGenerate = shift;
	my $triangleNumber = ($triangleNumToGenerate**2+$triangleNumToGenerate)/2;
	return $triangleNumber;
}

#calculate the number of divisors in for any number
sub getDivisorCount($)
{
	my $triangleNumber = shift;
		
	my $totalDivisors = 1;			#total number of divisors
	my $currentDivisor = 2;			#current number to divide by
	my $divisorUseCount = 0;		#the number of times $currentDivisor has been used
	while ($currentDivisor <= $triangleNumber)
	{
		#able to still divide by the current divisor
		if ($triangleNumber % $currentDivisor == 0)
		{
			$triangleNumber /= $currentDivisor;
			$divisorUseCount += 1;
		}
		#otherwise increment the divisor and the number of times it was used
		else
		{
			$currentDivisor += 1;
			$totalDivisors *= ($divisorUseCount+1);
			$divisorUseCount = 0;
		}
	}
	return 2*$totalDivisors;
}

sub main($)
{
	my $divisorsWanted = shift;
	my $currentTriangleNum = 10000;
	my $genTriangleNum = 0;
	my $divisorsFound = 0;

	while (1)
	{
		$genTriangleNum = generateNthTriangleNum($currentTriangleNum);
		$divisorsFound = getDivisorCount($genTriangleNum);
		$currentTriangleNum += 1;
		last if ($divisorsFound > $divisorsWanted);
	}
	print "$genTriangleNum has $divisorsFound divisors\n";
}

main(500);