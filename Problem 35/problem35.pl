#Desc:    Solution for Problem 35 (Circular Primes) on Project Euler
#Author:  Ryan Waskiewicz
#Date:    12/23/2013
#Problem: The number, 197, is called a circular prime because all rotations of
######### the digits: 197, 971, and 719, are themselves prime.  There are 
######### thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 
######### 73, 79, and 97. How many circular primes are there below one million?

use warnings;
use strict;
use POSIX;

# Determine if a number is prime or not
sub isPrime($)
{
	my $x = shift;
	if (($x % 2 == 0 && $x != 2) || $x == 1) {
		return 0;
	}
	elsif ($x < 9) {
		return 1;
	}

	my $squareRootX = floor(sqrt($x));
	for (my $indice = 3; $indice < $squareRootX+1; $indice += 2) {
		if ($x % $indice==0) {
			return 0;
		}
	}
	return 1;
}

sub findCircularPrimes($)
{
	my $currentValue = shift;

	if ($currentValue =~ m/2|4|5|8/ && $currentValue != 5 && $currentValue != 2) {
		return 0;
	}

	for (my $i = 0; $i < length($currentValue); $i++) {
		my $result = isPrime($currentValue);
		if (!$result) {
			return 0;
		}

		$currentValue = substr($currentValue, 1, length($currentValue)-1) . 
						substr($currentValue, 0, 1);
	}
	return 1;
}

sub iterateThroughValues($)
{
	my $highestValue = shift;
	my $numberOfCircular = 0;
	my $startTime = time();

	for (my $i = 0; $i < $highestValue; $i++) {
		$numberOfCircular += findCircularPrimes($i);
	}

	my $endTime = time() - $startTime;
	print "Found there are $numberOfCircular circular primes below $highestValue in $endTime seconds\n";
}

iterateThroughValues(1000000);